import discord
import api_tokens
import os
import yt_dlp
import ffmpeg
from discord import FFmpegPCMAudio
from discord.ext import commands

file = open('memberIDs.txt', 'r')
memberIDs = list(map(int, file.read().splitlines()))
file.close()

class Hero:
    def __init__(self, dataRow):
        dataRow = dataRow.split('=')
        self.nickNames  = dataRow[0].split('-')
        self.officialName = self.nickNames[0]
        self.nickNames = [h.lower() for h in self.nickNames]
        self.counters = dataRow[1].strip()
        
heroes = []
with open('ow2.txt', 'r') as f:
    fullList = f.read().splitlines()
f.close()
for dataRow in fullList:
    heroes.append(Hero(dataRow))

prefix = '.'
client = commands.Bot(command_prefix=prefix, help_command=None, intents=discord.Intents().all())

@client.event
async def on_ready():
    print('{0} is ready to fight!'.format(client.user))
    await client.change_presence(status=discord.Status.online, activity=discord.Game("Marine Bio Sim"))

@client.event
async def on_message(message):
    if message.author.bot: # skip message if from a bot
            return

    await client.process_commands(message)

    context = await client.get_context(message)
    if message.guild == None or context.valid: # only send message if the @ came from a guild and is not a command
        return

    mentionedList = []
    for mentioned in message.mentions:
        if not mentioned.bot and mentioned.id in memberIDs: # see if the mentioned person is in the member list
            embed = discord.Embed()
            embed.description = '{0}\nFrom: {1}, In: [{2}]({3})'.format(message.content, message.author.display_name, message.guild.name, message.jump_url)
            await mentioned.send(embed=embed)
            mentionedList.append(mentioned.display_name)
    count = len(mentionedList)
    if count == 0: return
    if count == 1:
        await message.channel.send("I'll let {0} know".format(mentionedList[0]))
        return
    if count == 2:
        await message.channel.send("I'll let {0} and {1} know".format(mentionedList[0], mentionedList[1]))
        return
    text = ""
    for i in range(0, count - 1):
        text += mentionedList[i] + ", "
    await message.channel.send("I'll let " + text + "and " + mentionedList[count - 1] + " know")    

@client.command()
async def jojo(ctx):
    if ctx.message.author.id in memberIDs:
        await ctx.send('Good grief, I already have you on the list')
        return
    else:
        memberIDs.append(ctx.message.author.id)
        file = open('memberIDs.txt', 'w')
        for memberID in memberIDs:
            file.write('{0}\n'.format(memberID))
        file.close()
        await ctx.send('I added you to my Pokedex {0}, use {1}nono to leave'.format(ctx.message.author.mention, prefix))
        print('Added:   {0}'.format(ctx.message.author.display_name))

@client.command()
async def nono(ctx):
    if ctx.message.author.id not in memberIDs:
        await ctx.send('Who are you?')
        return
    else:
        memberIDs.remove(ctx.message.author.id)
        file = open('memberIDs.txt', 'w')
        for memberID in memberIDs:
            file.write('{0}\n'.format(memberID))
        file.close()
        await ctx.send('I guess Star Platinum was too powerful for you, {0}'.format(ctx.message.author.mention))
        print('Removed: {0}'.format(ctx.message.author.display_name))

@client.command()
async def swap(ctx):
    if ctx.message.author.id != 324344176944873472: # only creator(squig) can swap
        return
    for mentioned in ctx.message.mentions:
        if mentioned.id not in memberIDs:
            memberIDs.append(mentioned.id)
            await ctx.send("Added {0}".format(mentioned.display_name))
        else:
            memberIDs.remove(mentioned.id)
            await ctx.send("Removed {0}".format(mentioned.display_name))
            file = open('memberIDs.txt', 'w')
            for memberID in memberIDs:
                file.write('{0}\n'.format(memberID))
            file.close()
            ctx.message.delete

@client.command()
async def help(ctx):
    await ctx.send('`Use "{0}jojo" to be added and "{0}nono" to be removed from notifications. Use .c "hero" to get a list of counters for an Overwatch2 hero!`'.format(prefix))
    
@client.command()
async def c(ctx, heroArg):
    for hero in heroes:
        if heroArg.lower() in hero.nickNames:
            await ctx.send('The counters to {0} are: {1}'.format(hero.officialName, hero.counters))
            return
    await ctx.send('No data found for {0}'.format(heroArg))

client.run(api_tokens.bot_token)